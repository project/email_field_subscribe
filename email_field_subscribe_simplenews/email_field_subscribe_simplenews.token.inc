<?php

/**
 * @file
 * Token related hook implementations.
 */

/**
 * Implements hook_token_info().
 */
function email_field_subscribe_simplenews_token_info() {
  // Tokens for email field simplenews subscriber.
  $info['tokens']['simplenews-subscriber']['email-field-node'] = array(
    'name' => t('Corresponding email field node'),
    'description' => t('The node object that corresponds to this subscriber. This is only set for email field subscribers.'),
    'type' => 'node',
  );
  $info['tokens']['simplenews-subscriber']['email-field-term'] = array(
    'name' => t('Corresponding email field term'),
    'description' => t('The term object that corresponds to this subscriber. This is only set for email field subscribers.'),
    'type' => 'term',
  );
  $info['tokens']['simplenews-subscriber']['email-field-user'] = array(
    'name' => t('Corresponding email field user'),
    'description' => t('The user object that corresponds to this subscriber. This is only set for email field subscribers.'),
    'type' => 'user',
  );

  return $info;
}

/**
 * Implements hook_tokens().
 */
function email_field_subscribe_simplenews_tokens($type, $tokens, $data = array(), $options = array()) {
  $replacements = array();

  switch ($type) {
    case 'simplenews-subscriber':
      if (isset($data['simplenews_subscriber'])) {
        $subscriber = $data['simplenews_subscriber'];
        $category = isset($data['category']) ? $data['category'] : NULL;

        $query = db_select('email_field_subscribe', 'r')
          ->fields('r', array('entity_id'))
          ->condition('mail', $subscriber->mail)
          ->condition('newsletter_id', $category->tid);

        $entity_ids = array();
        foreach ($query->execute() as $result) {
          $entity_ids[] = $result->entity_id;
        }

        if (!empty($entity_ids) && ($node_tokens = token_find_with_prefix($tokens, 'email-field-node')) && isset($subscriber->uid)) {
          $entities = entity_load('node', $entity_ids);
          $values = array();
          foreach ($entities as $entity) {
            $values[$entity->nid] = token_generate('node', $node_tokens, array('node' => $entity), $options);
          }

          $replacements += _email_field_subscribe_tokens_format($node_tokens, $values);
        }

        if (!empty($entity_ids) && ($node_tokens = token_find_with_prefix($tokens, 'email-field-term')) && isset($subscriber->uid)) {
          $entities = entity_load('term', $entity_ids);
          $values = array();
          foreach ($entities as $entity) {
            $values[$entity->nid] = token_generate('term', $node_tokens, array('term' => $entity), $options);
          }

          $replacements += _email_field_subscribe_tokens_format($node_tokens, $values);
        }

        if (!empty($entity_ids) && ($node_tokens = token_find_with_prefix($tokens, 'email-field-user')) && isset($subscriber->uid)) {
          $entities = entity_load('user', $entity_ids);
          $values = array();
          foreach ($entities as $entity) {
            $values[$entity->nid] = token_generate('user', $node_tokens, array('user' => $entity), $options);
          }

          $replacements += _email_field_subscribe_tokens_format($node_tokens, $values);
        }
      }

      break;
  }

  return $replacements;
}
