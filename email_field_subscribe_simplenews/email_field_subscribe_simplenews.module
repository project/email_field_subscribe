<?php

/**
 * @file
 * Implements email_field_subscribe_simplenews functionality.
 */

/**
 * Helper function to add Simplenews options to email subscriptions settings.
 *
 * @param array $settings
 *   Form instance widget settings.
 *
 * @return mixed
 *   Array of form fields for to add to email subscriptions settings form.
 */
function _email_field_subscribe_simplenews_field_widget_settings_form(array $settings) {
  // Load all Simplenews newsletter categories.
  if ($categories = simplenews_categories_load_multiple(array(), array('show_all' => TRUE))) {
    // Create options from a newsletters.
    $options = array();
    foreach ($categories as $category) {
      $options[$category->tid] = check_plain(_simplenews_newsletter_name($category));
    }
    // Check if newsletter categories have been deleted.
    $default_value = array();
    foreach ($settings['email_field_subscribe']['simplenews_newsletters'] as $key => $value) {
      if (array_key_exists($key, $options)) {
        $default_value[] = $value;
      }
    }

    $form['simplenews_newsletters'] = array(
      '#type' => 'select',
      '#title' => t('Simplenews subscriptions'),
      '#multiple' => TRUE,
      '#options' => $options,
      '#default_value' => $default_value,
      '#description' => t('Choose the subscriptions to automatically update.'),
      '#states' => array(
        'visible' => array(
          ':input[name="instance[widget][settings][email_field_subscribe][enabled]"]' => array('checked' => TRUE),
        ),
      ),
    );
  }
  else {
    $form['simplenews_newsletters'] = array(
      '#type' => 'markup',
      '#value' => t('<a href="@create-simplenews-newsletter">Create a Simplenews newsletter</a>.', array(
        '@create-simplenews-newsletter' => url('admin/config/services/simplenews'),
      )),
      '#states' => array(
        'visible' => array(
          ':input[name="instance[widget][settings][email_field_subscribe][enabled]"]' => array('checked' => TRUE),
        ),
      ),
    );
  }

  return $form;
}

/**
 * Implements hook_form_alter().
 */
function email_field_subscribe_simplenews_form_alter(&$form, &$form_state, $form_id) {
  switch ($form_id) {

    // Delete newsletter.
    case 'simplenews_admin_category_delete':
      // Make sure our submit callback executes first.
      array_unshift($form['#submit'], '_email_field_subscribe_simplenews_admin_category_delete');
      break;

    // Mass unsubscribe.
    case 'simplenews_subscription_list_remove':
      $form['#submit'][] = '_email_field_subscribe_simplenews_subscription_list_remove';
      break;

    // Subscriber edit form.
    case 'simplenews_subscriptions_admin_form':
      // Make sure our submit callback executes first.
      array_unshift($form['#submit'], '_email_field_subscribe_simplenews_subscriptions_admin_form');
      break;
  }
}

/**
 * Handle multiple deleting on newsletter remove.
 */
function _email_field_subscribe_simplenews_admin_category_delete($form, &$form_state) {
  if ($form_state['values']['confirm']) {
    $results = db_select('email_field_subscribe', 'r')
      ->fields('r')
      ->condition('r.newsletter_id', $form_state['values']['tid'])
      ->execute();

    foreach ($results as $result) {
      _email_field_subscribe_set_entity_data($result->mail, $result->newsletter_id, $result->entity_id, TRUE);
    }
  }
}

/**
 * Handle submission of the mass unsubscription form and deleting.
 */
function _email_field_subscribe_simplenews_subscription_list_remove($form, &$form_state) {
  $emails = preg_split("/[\s,]+/", $form_state['values']['emails']);
  foreach ($emails as $mail) {
    // Prevent mismatches from accidental capitals in mail address.
    $mail = strtolower(trim($mail));
    // Get subscription account information.
    $subscriber = simplenews_subscriber_load_by_mail($mail);
    if ($subscriber) {
      foreach ($form_state['values']['newsletters'] as $newsletter_id => $disable) {
        if ($disable) {
          _email_field_subscribe_set_entity_data($mail, $newsletter_id, NULL, TRUE);
        }
      }
    }
  }
}

/**
 * Handle multiple deleting on newsletter remove.
 */
function _email_field_subscribe_simplenews_subscriptions_admin_form($form, &$form_state) {
  switch ($form_state['values']['op']) {
    case 'Update':
      $mail = $form_state['values']['mail'];
      foreach ($form_state['values']['newsletters'] as $newsletter_id => $disabled) {
        if (empty($disabled)) {
          _email_field_subscribe_set_entity_data($mail, $newsletter_id, NULL, TRUE);
        }
      }
      break;
  }
}

/**
 * Implements hook_simplenews_subscriber_delete().
 */
function email_field_subscribe_simplenews_subscriber_delete($subscriber) {
  db_delete("email_field_subscribe")
    ->condition('mail', $subscriber->mail)
    ->execute();
}

/**
 * Helper function to update Simplenews newsletter.
 *
 * @param array $args
 *   In the form of...
 *   mail(array) - address of the user - key email
 *   entity(object) - entity
 *   tids(array) - numerical tids for newsletters
 *   language(string) - language of newsletter
 *   operation(string) for operation to perform (subscribe or unsubscribe).
 */
function _email_field_subscribe_simplenews_update_subscriptions(array $args) {
  if (isset($args['operation'])) {
    if (!is_array($args['newsletter_ids'])) {
      $args['newsletter_ids'] = array($args['newsletter_ids']);
    }
    if (isset($args['language']) && $args['language'] == LANGUAGE_NONE) {
      $args['language'] = '';
    }
    // Subscribe to all newsletters selected.
    foreach ($args['newsletter_ids'] as $tid) {
      // Load Simplenews category to ensure that it exists.
      if ($category = simplenews_category_load($tid)) {
        switch ($args['operation']) {

          case 'subscribe':
            // If category is found then add to newsletter.
            $category = simplenews_category_load($tid);
            if ($category) {
              // Would prefer to use simplenews subscribe function.
              // Can not because simplenews will associate with a user.
              // Subscriber should not be associated with a user.
              simplenews_subscribe_user($args['mail'], $category->tid, FALSE, 'email_field', $args['language']);

              // Get current subscriptions if any.
              $subscriber = simplenews_subscriber_load_by_mail($args['mail']);
              // If user is not subscribed to ANY newsletter, create a subscription account
              if (!$subscriber) {
                $subscriber = new stdClass();
                $subscriber->mail = $args['mail'];
                $subscriber->uid = 0;
                $subscriber->language = $args['language'];
                $subscriber->activated = 1;
                simplenews_subscriber_save($subscriber);
              }
              if (!isset($subscriber->tids[$tid])) {
                // Subscribe the user if not already subscribed.
                $subscription = new stdClass();
                $subscription->snid = $subscriber->snid;
                $subscription->tid = $tid;
                $subscription->status = SIMPLENEWS_SUBSCRIPTION_STATUS_SUBSCRIBED;
                $subscription->timestamp = REQUEST_TIME;
                $subscription->source = 'email_field';
                simplenews_subscription_save($subscription);
                $subscriber->newsletter_subscription[$tid] = $subscription;
                $subscriber->tids[$tid] = $tid;
                // Still invoke hook to not break functionality with Simplenews.
                module_invoke_all('simplenews_subscribe_user', $subscriber, $subscription);
              }
              // Add subscriber data to email_field_subscribe table.
              _email_field_subscribe_set_entity_data($args['mail']['email'], $tid, $args['entity']->nid);
            }
            break;

          case 'unsubscribe':
            // Remove subscriber data from email_field_subscribe table.
            _email_field_subscribe_set_entity_data($args['mail']['email'], $tid, $args['entity']->nid, TRUE);
            // Check is any other entities are subscribed to newsletter.
            if (empty(_email_field_subscribe_has_entity_data($args['mail']['email'], $category->tid))) {
              simplenews_unsubscribe_user($args['mail'], $category->tid, FALSE, 'email_field');
            }
            break;
        }
      }
    }
  }
}
