CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Troubleshooting
 * FAQ
 * Maintainers

INTRODUCTION
------------

The Email subscriptions module allows entites to be automatically subscribed and
un-subscribed from subscriptions (newsletters).

Currently, the module supports Simplenews, and can very easily support more
subscription modules. Create an issue on the project page issue if you would
like another subscription module supported. You can also submit a patch to
support another subscription module at the issue page for this project.

 * For a full description of the module, visit the project page:
   https://drupal.org/project/email_field_subscribe

 * To submit bug reports and feature suggestions, or to track changes:
   https://drupal.org/project/issues/email_field_subscribe

REQUIREMENTS
------------

This module requires the following modules:

 * Email (https://www.drupal.org/project/email)
 * Simplenews (https://www.drupal.org/project/simplenews)

 * Tokens (https://www.drupal.org/project/token) (Optional)

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

 * Install Email Field module and enable. See:
   https://www.drupal.org/project/email

 * Install Simplenews module and enable. See:
   https://www.drupal.org/project/simplenews

RECOMMENDED MODULES
-------------------

 * None (ATM).

CONFIGURATION
-------------

 * Configure email subscription options on settings page for the email CCK
   field.

   - Check "Automatically add email to subscriptions" to turn on.

   - Check the subscription update actions for when the auto subscribe and
     un-subscribe actions should be performed. Current auto subscribe actions
     are on entity creation and update. Current auto un-subscribe action is on
     entity deletion.

   - Select the subscriptions to be updated on subscription update actions.
     More than on subscription may be selected.

TROUBLESHOOTING
---------------

None.

FAQ
---

 * Entities with identical email addresses will behave as the following
   example:

   Entity A has email address email@address.com.
   Entity B has email address email@address.com.
   Entity A and B has been subscribed to List C.

   FOR UPDATES

   Step 1: Update A to email2@address.com.
    - email2@address.com added to C.
    - email@address.com remains subscribed to C because of B.

   Step 2. Update B to email3@address.com.
    - email3@address.com added to C.
    - email@address removed from C.

   FOR DELETES

   Step 1: Delete A.
    - email@address.com remains subscribed to C because of B.

   Step 2. Delete B.
    - email@address removed from C.

 * Tokens support for Simplenews:

   Supported in Simplenews newsletter content types.

   Subscribed Node: [simplenews-subscriber][email-field-node]
   Subscribed Term: [simplenews-subscriber][email-field-term]
   Subscribed User: [simplenews-subscriber][email-field-user]

MAINTAINERS
-----------

Current maintainers:
 * Brian Glass (monstrfolk) - https://www.drupal.org/u/monstrfolk
